module codeberg.org/gruf/go-xgb

go 1.18

require (
	codeberg.org/gruf/go-byteutil v1.1.2
	codeberg.org/gruf/go-xgbutil v1.0.0
)
